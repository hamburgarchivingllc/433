# 433

A programming language without any predefined operators, expressions, functions, or classes. Source code for programs written in 433 should use the .433 filename suffix.